import java.io.*;

class mergeSort{

    void merge(int arr[], int l, int m, int r){   
        int sub1 = m - l + 1;
        int sub2 = r - m;

        int L[] = new int[sub1];
        int R[] = new int[sub2];

        for(int i = 0; i < sub1; ++i){
            L[i] = arr[l + i];
        }
        for(int j = 0; j < sub2; ++j){
            L[j] = arr[m + 1 + j];        
        }

        int i = 0, j = 0;

        int k = l;
        while(i < sub1 && j < sub2){
            if(L[i] <= R[i]){
                arr[k] = L[i];
                i++;
            }
            else{
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        while (i < sub1) {
            arr[k] = L[i];
            i++;
            k++;
        }
        while (j < sub2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    void sort(int arr[], int l, int r){
        if(l < r){
            int m = l + (r - l) / 2;
            sort(arr, l, m);
            sort(arr, m + 1, r);

            merge(arr, l, m, r);
        }
    }

    public static void main(String args[]){

        int arr[] = { 12, 11, 13, 5, 6, 7 };
        sort(arr, 0, arr.length - 1);
    }
}